"use strict"
let con = document.getElementById("content")
// loginLoad()

function loginLoad() {
    let loginForm = document.createElement("div")
    loginForm.appendChild(logintmplt.content.cloneNode(true))
    con.appendChild(loginForm)
}

function login(node) {
    const errorMessage = node.querySelector("#errorMessage")
    errorMessage.style.visibility = "hidden"
    let account = node.querySelector("#userAccount").value.replace(/'/g, "\\'")
    let password = node.querySelector("#userPassword").value.replace(/'/g, "\\'")

    fetch("../api/login", { method: "POST", body: JSON.stringify({ account, password }) }).then((response) => {
        console.log("resp: ", response)
        response.json().then((result) => {
            if (typeof result === "string" && result.includes("Error")) {
                console.log("whats wrong with result? ", result)
                errorMessage.style.visibility = "visible"
            } else {
                const { id, sessionKey, name, department } = result
                let date = new Date()
                date.setHours(date.getHours() + 3)
                document.cookie = `${id}=${sessionKey}-${name}-mayjs; expires=${date.toUTCString()}; path=/`

                window.location.replace("/")
            }
        })
    })
}

function clearFields(node) {
    node.querySelector("#user-account").value = ""
    node.querySelector("#user-password").value = ""
}

function register(node) {
    console.log("node: ", node.userAccount)
    let account = node.userAccount.value.replace(/'/g, "\\'")
    let password = node.userPassword.value.replace(/'/g, "\\'")
    let email = node.userEmail.value.replace(/'/g, "\\'")
    let department = node.userDepartment.value.replace(/'/g, "\\'")
    let description = node.userDescription.value.replace(/'/g, "\\'")
    let name = node.userName.value.replace(/'/g, "\\'")

    fetch("../api/registration", { method: "POST", body: JSON.stringify({ account, name, email, password, description, department }) }).then((response) => {
        console.log("resp: ", response)

        response.json().then((result) => {
            console.log("result: ", result)
            if (typeof result === "string" && result.includes("Error")) {
                console.log("err message: ", result)
                errorMessage.style.visibility = "visible"
                errorMessage.innerHTML = "Something wrong, try different account and password"
            } else {
                window.location.replace("/page/login")
            }
        })
    })
}
