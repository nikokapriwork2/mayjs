"use strict"

let con = document.getElementById("content")
let account = ""
let username = ""
let email = ""
let department = ""
let description = ""
const cookieArr = document.cookie.split(";")
const myCookie = cookieArr.filter((item) => item.includes("mayjs"))[0]
const sessionKey = myCookie.split("=")[1].split("-")[0]

fetch("../api/getusers", { method: "GET" }).then((response) => {
    response.json().then((users) => {
        for (let user of users) {
            let userElem = document.createElement("div")
            userElem.classList.add("col-sm-6")
            userElem.appendChild(usercardtmlpt.content.cloneNode(true))
            console.log("user elem: ", userElem.children[0].children[0])
            userElem.querySelector("#userId").style.display = "none"
            userElem.querySelector("#userId").innerHTML = `u-${user.id}`
            userElem.querySelector("#blockBtn").style.display = user.status ? "inline-block" : "none"
            userElem.querySelector("#unblockBtn").style.display = !user.status ? "inline-block" : "none"

            userElem.querySelector("#blockBtn").style.visibility = sessionKey === user.session_key ? "hidden" : myCookie.match(/Ad\d+\D+-mayjs/) ? "visible" : "hidden"
            userElem.querySelector("#editBtn").style.visibility = sessionKey === user.session_key ? "visible" : myCookie.match(/Ad\d+\D+-mayjs/) ? "visible" : "hidden"

            userElem.querySelector("#userAccount").innerHTML = `${user.account}`
            userElem.querySelector("#userName").innerHTML = `${user.name}`
            userElem.querySelector("#userEmail").innerHTML = `${user.email}`
            userElem.querySelector("#userDepartment").innerHTML = `${user.department}`
            userElem.querySelector("#userDescription").innerHTML = `${user.description}`

            con.appendChild(userElem)
        }
    })
})

function editUser(element) {
    console.log("element: ", element)
    let id = element.querySelector("#userId").innerHTML.match(/\d+/)[0]
    console.log("id: ", id)
    let userElem = document.createElement("div")
    userElem.style.display = "table-caption"
    console.log("tmlt : ", editusertmplt)
    userElem.appendChild(editusertmplt.content.cloneNode(true))
    userElem.querySelector("#userId").style.display = "none"
    userElem.querySelector("#userId").innerHTML = id

    userElem.querySelector("#userAccount").value = account = element.querySelector("#userAccount").innerHTML
    userElem.querySelector("#userName").value = username = element.querySelector("#userName").innerHTML
    userElem.querySelector("#userEmail").value = email = element.querySelector("#userEmail").innerHTML

    const departmentSelect = userElem.querySelector("#userDepartment")
    departmentSelect.value = department = element.querySelector("#userDepartment").innerHTML
    if (myCookie.match(/Ad\d+\D+-mayjs/)) {
        let opt = document.createElement("option")
        opt.value = "Admin"
        opt.innerHTML = "Admin"
        if (myCookie.includes(username)) {
            opt.setAttribute("selected", "true")
            departmentSelect.setAttribute("disabled", "true")
        }
        departmentSelect.appendChild(opt)
    }

    userElem.querySelector("#userDescription").value = description = element.querySelector("#userDescription").innerHTML

    console.log("options of selector: ", userElem.querySelector("#userDepartment").options)
    console.log("department value: ", department)

    con.innerHTML = ""
    con.appendChild(userElem)
}
function updateUser(element) {
    let id = element.querySelector("#userId").innerHTML.replace(/'/g, "\\'")
    let newAccount = element.userAccount.value.replace(/'/g, "\\'")
    let newPassword = element.userPassword.value.replace(/'/g, "\\'")
    let newEmail = element.userEmail.value.replace(/'/g, "\\'")
    let newDepartment = element.userDepartment.value.replace(/'/g, "\\'")
    let newDescription = element.userDescription.value.replace(/'/g, "\\'")
    let newName = element.userName.value.replace(/'/g, "\\'")

    if (myCookie.includes(username)) {
        // const check = confirm("")
        let [cookieName, cookieValue] = myCookie.split("=")
        console.log("replacing value ", cookieValue.replace(account, newAccount))
        let date = new Date()
        date.setHours(date.getHours() + 3)
        document.cookie = `${cookieName}=${cookieValue.replace(username, newName)};Expires=${date.toUTCString()};Path=/`
    }
    console.log("data: ", id, newAccount, newName, newEmail, newPassword, newDepartment, newDescription)
    fetch("../api/updateuser", { method: "POST", body: JSON.stringify({ id, newAccount, newName, newEmail, newPassword, newDepartment, newDescription }) }).then((response) => {
        response.json().then((result) => {
            if (typeof result === "string" && result.includes("Error")) {
                errorMessage.style.visibility = "visible"
                errorMessage.innerHTML = "Something wrong, try different account and password"
            } else {
                document.location.reload()
            }
        })
    })
}
function cancelValues(element) {
    element.querySelector("#userAccount").value = account
    element.querySelector("#userName").value = username
    element.querySelector("#userEmail").value = email
    element.querySelector("#userDepartment").value = department
    element.querySelector("#userDescription").value = description
    element.querySelector("#userPassword").value = ""
}
function blockUser(element) {
    let id = element.querySelector("#userId").innerHTML.match(/\d+/)[0]
    element.querySelector("#blockBtn").style.display = "none"
    element.querySelector("#unblockBtn").style.display = "inline-block"

    fetch("../api/change-status", { method: "POST", body: JSON.stringify({ id, status: 0 }) }).then((response) => {})
}

function unblockUser(element) {
    let id = element.querySelector("#userId").innerHTML.match(/\d+/)[0]
    element.querySelector("#unblockBtn").style.display = "none"
    element.querySelector("#blockBtn").style.display = "inline-block"

    fetch("../api/change-status", { method: "POST", body: JSON.stringify({ id, status: 1 }) }).then((response) => {})
}
