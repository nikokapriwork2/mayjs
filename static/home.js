"use strict"

let con = document.getElementById("content")
let messages
let comments
const cookieArr = document.cookie.split(";")
const myCookie = cookieArr.filter((item) => item.includes("mayjs"))[0]
const sessionKey = myCookie.split("=")[1].split("-")[0]

fetch("../api/get-messages-and-comments", { method: "GET" }).then((response) => {
    response.json().then((result) => {
        console.log("result: ", result)
        messages = result.messages
        comments = result.comments

        showMessagesAndComments(messages, comments)
    })
})

function showMessagesAndComments(messages, comments) {
    con.innerHTML = ""
    for (let message of messages) {
        // console.log("message: ", message)
        let messageElem = document.createElement("div")

        messageElem.appendChild(messagetmplt.content.cloneNode(true))
        messageElem.querySelector("#message-date").innerHTML = message.created_date.replace("T", " ").replace("Z", "").replace(".000", "")
        messageElem.querySelector("#message-category").innerHTML = message.category
        messageElem.querySelector("#message-text").innerHTML = message.text
        messageElem.querySelector("#message-user").innerHTML = message.name

        messageElem.querySelector("#msgDelBtn").style.display = myCookie.match(/Ad\d+\D+-mayjs/) ? "true" : sessionKey === message.session_key ? "true" : "none"
        messageElem.querySelector("#msgDelBtn").setAttribute("id", `db-${message.id}`)
        messageElem.querySelector("#cmtSdBtn").setAttribute("id", `cs-${message.id}`)
        messageElem.setAttribute("id", `m-${message.id}`)

        for (let comment of comments) {
            // console.log("comment: ", comment)
            if (comment.message_id === message.id) {
                let commentElem = document.createElement("li")
                commentElem.classList.add("list-group-item")
                console.log("message elem ul")

                commentElem.appendChild(commenttmplt.content.cloneNode(true))
                commentElem.querySelector("#comment-date").innerHTML = comment.created_date.replace("T", " ").replace("Z", "").replace(".000", "")
                commentElem.querySelector("#comment-text").innerHTML = comment.text
                commentElem.querySelector("#comment-user").innerHTML = comment.name
                console.log("comment", comment, "session key: ", comment.session_key)
                commentElem.querySelector("#cmtDelBtn").style.display = myCookie.match(/Ad\d+\D+-mayjs/) ? "true" : sessionKey === comment.session_key ? "true" : "none"
                commentElem.querySelector("#cmtDelBtn").setAttribute("id", `cd-${comment.id}`)

                commentElem.setAttribute("id", `c-${comment.id}`)
                messageElem.querySelector("#commentBody").appendChild(commentElem)
            }
        }

        con.appendChild(messageElem)
    }
}
function deleteMsg(id) {
    fetch("api/deletemessage", { method: "POST", body: JSON.stringify(id) }).then(() => document.getElementById(`m-${id}`).remove())
}
function sendComment(messageId, textNode) {
    let errorMessage = document.querySelector("#errorMessage")
    errorMessage.style.visibility = "hidden"
    let text = textNode.value.replace(/'/g, "\\'")
    if (text.length < 1 || text.length > 100) {
        console.log("length error!")
        errorMessage.style.visibility = "visible"
        errorMessage.innerHTML = "Comment should be more than 1 character and less then 100 characters!"
        return
    }
    console.log("text: ", text)
    const [myId, myValue] = myCookie.split("=")

    fetch("api/sendcomment", {
        method: "POST",
        body: JSON.stringify({ messageId, text, myId })
    }).then((response) => {
        response.json().then((id) => {
            let messageElem = document.getElementById(`m-${messageId}`)

            let commentElem = document.createElement("li")
            commentElem.classList.add("list-group-item")
            let date = new Date()
            let current = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours() - 9}:${date.getMinutes()}:${date.getSeconds()}`
            commentElem.appendChild(commenttmplt.content.cloneNode(true))
            commentElem.querySelector("#comment-date").innerHTML = current
            commentElem.querySelector("#comment-text").innerHTML = text

            commentElem.querySelector("#comment-user").innerHTML = myValue.split("-")[1]
            commentElem.setAttribute("id", `c-${id}`)
            commentElem.querySelector("#cmtDelBtn").setAttribute("id", `cd-${id}`)
            messageElem.querySelector("#commentBody").prepend(commentElem)
            textNode.value = ""
        })
    })
}
function deleteCmt(id) {
    fetch("api/deletecomment", { method: "POST", body: JSON.stringify(id) }).then(() => {
        document.getElementById(`c-${id}`).remove()
    })
}

function filterMessages(element) {
    let date = `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date().getDate()}`

    let start = element.querySelector("#dateStart").value ? element.querySelector("#dateStart").value : "1999-01-12"
    let end = element.querySelector("#dateEnd").value ? element.querySelector("#dateEnd").value : date
    let category = element.querySelector("#categorySearch").value
    console.log("start end: ", start, end)
    let filteredMessages = messages
        .filter((item) => item.created_date >= start && item.created_date <= end)
        .filter((item) => (category.trim().length > 0 ? item.category.toLowerCase().includes(category.toLowerCase()) : true))
        .sort((a, b) => new Date(b.created_date) - new Date(a.created_date))

    showMessagesAndComments(filteredMessages, comments)
}
function clearSearch(element) {
    console.log("clear")
    for (let input of element.querySelectorAll("input")) {
        console.log("input value: ", input.value)
        input.value = ""
    }
    showMessagesAndComments(messages, comments)
}
