const db = require("./db")

module.exports = function (cookie) {
    console.log("filter cookie: ", cookie)

    if (!cookie) return false
    const cookieArr = cookie.split(";")
    const myCookie = cookieArr.filter((item) => item.includes("mayjs"))[0]

    const [id, value] = myCookie.split("=")
    const [sessionKey] = value.split("-")

    console.log("account? ", sessionKey)
    return db("filter", { id, sessionKey })
        .then((result) => {
            console.log("filtering was true")
            return true
        })
        .catch(() => {
            console.log("false")
            return false
        })
}
