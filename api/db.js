const mysql = require("mysql")
crypto = require("crypto")

// const secret = "abcdefg"
// const hash = createHmac("sha256", secret).update("Nikolay crypto thing").digest("hex")
// console.log(hash)

module.exports = function (reason, payload) {
    console.log("reason :", reason)
    const host = "localhost"
    const user = "root"
    const password = "root"
    const database = "dashboard"
    const con = mysql.createConnection({
        host: host,
        user: user,
        password: password,
        database: database
    })
    return new Promise((resolve, reject) => {
        //----------------Get messages and comments---------------------------
        if (reason.match(/get-messages-and-comments$/)) {
            con.connect(function (err) {
                if (err) throw err
                con.query("SELECT messages.*, users.name, users.session_key FROM messages LEFT JOIN users ON messages.user_id=users.id ORDER BY created_date DESC", function (err, messages, fields) {
                    // console.log("messages date: ", messages)
                    if (err) throw err
                    con.query("SELECT comments.*, users.name, users.session_key FROM comments LEFT JOIN users ON comments.user_id=users.id ORDER BY created_date DESC;", function (err, comments, fields) {
                        // console.log("comments:", comments)
                        if (err) throw err
                        resolve({ messages, comments })
                    })
                })
            })
            //----------------Delete message---------------------------
        } else if (reason.match(/deletemessage$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body += chunk
                })
                .on("end", () => {
                    const messageId = body.replace(/\"/g, "'")
                    con.connect(function (err) {
                        if (err) throw err
                        con.query(`DELETE FROM messages WHERE id = ${messageId}`, (err, result) => {
                            if (err) reject(err)
                            con.query(`DELETE FROM comments WHERE message_id = ${messageId}`, (err, result) => {
                                if (result) resolve("success, u may resolve")
                                if (err) reject(err)
                            })
                        })
                    })
                })
            //----------------Delete comment---------------------------
        } else if (reason.match(/deletecomment$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body += chunk
                })
                .on("end", () => {
                    const commentId = body.replace(/\"/g, "'")
                    con.connect(function (err) {
                        if (err) throw err
                        let sql = `DELETE FROM comments WHERE id = ${commentId}`
                        con.query(sql, (err, result) => {
                            if (result) resolve("success, u may resolve")
                            if (err) reject(err)
                        })
                    })
                    resolve("success")
                })
            //----------------Send message---------------------------
        } else if (reason.match(/sendmessage$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body = JSON.parse(chunk)
                })
                .on("end", () => {
                    console.log("message body: ", body)
                    let { id, category, text } = body
                    category = category.replace(/'/g, "\\'")
                    text = text.replace(/'/g, "\\'")

                    con.connect(function (err) {
                        if (err) throw err
                        let sql = `INSERT INTO messages (user_id, category, text) VALUES ('${id.split("=")[0]}', '${category}', '${text}')`
                        con.query(sql, (err, result) => {
                            console.log("result message: ", result)
                            console.log("err: ", err)
                            if (result) resolve("success, u may resolve")
                            if (err) reject(err)
                        })
                    })
                })
            //----------------Send comment---------------------------
        } else if (reason.match(/sendcomment$/)) {
            let comment = {}
            payload
                .on("data", (chunk) => {
                    comment = JSON.parse(chunk)
                })
                .on("end", () => {
                    con.connect(function (err) {
                        let { messageId, text, myId: userId } = comment
                        if (err) throw err
                        let sql = `INSERT INTO comments (user_id, message_id, text) VALUES (${userId}, '${messageId}', '${text}')`
                        con.query(sql, (err, result) => {
                            console.log("comment result: ", result)
                            console.log("comment err: ", err)
                            let { insertId } = result
                            if (result) resolve(insertId)
                            if (err) reject(err)
                        })
                    })
                })
        } else if (reason.match(/getusers$/)) {
            con.connect(function (err) {
                if (err) throw err
                con.query("SELECT * FROM users", function (err, users, fields) {
                    if (err) throw err

                    resolve(users)
                })
            })
            //----------------Edit user---------------------------
        } else if (reason.match(/updateuser$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body = JSON.parse(chunk)
                })
                .on("end", () => {
                    let { id, newAccount, newName, newEmail, newPassword, newDepartment, newDescription } = body

                    if (newPassword) newPassword = crypto.createHmac("sha256", newPassword).update("Nikolay's crypto thing").digest("hex")

                    // console.log("body user update: ", id, newAccount, newName, newEmail, newPassword, newDepartment, newDescription)
                    const queryWithPW = `UPDATE users SET account='${newAccount}', name='${newName}', email='${newEmail}', password='${newPassword}', department='${newDepartment}', description='${newDescription}' WHERE id='${id}'`
                    const queryWithoutPW = `UPDATE users SET account='${newAccount}', name='${newName}', email='${newEmail}', department='${newDepartment}', description='${newDescription}' WHERE id='${id}'`
                    con.connect(function (err) {
                        console.log("passw: ", !newPassword.length)
                        const sql = !newPassword.length ? queryWithoutPW : queryWithPW
                        if (err) throw err
                        // console.log("trying sql query")
                        con.query(sql, (err, result) => {
                            if (err) reject(err)
                            resolve(result)
                        })
                    })
                })
            //----------------Login user---------------------------
        } else if (reason.match(/login$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body = JSON.parse(chunk)
                })
                .on("end", () => {
                    let { account, password } = body
                    password = crypto.createHmac("sha256", password).update("Nikolay's crypto thing").digest("hex")
                    con.connect(function (err) {
                        if (err) throw err
                        // console.log("trying sql query")
                        console.log("acc: ", account, "password: ", password)
                        con.query(`SELECT * FROM users WHERE account='${account}' AND password='${password}'`, (err, result) => {
                            if (err) reject(err)
                            console.log("result: ", result)
                            if (result && result.length) {
                                const { id, account, name, department, status } = result[0]
                                let sessionKey = `${department.slice(0, 2)}${Math.trunc(Math.random() * 100000)}`
                                con.query(`UPDATE users SET session_key='${sessionKey}' WHERE id='${id}'`, (err, result) => {
                                    if (err) reject(err)
                                    if (!status) reject("Error: Invalid password or account")
                                    resolve({ id, account, name, department, sessionKey })
                                })
                            } else reject("Error: Invalid password or account")
                        })
                    })
                })
            //----------------Registration user---------------------------
        } else if (reason.match(/registration$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body = JSON.parse(chunk)
                })
                .on("end", () => {
                    let { account, name, email, password, description, department } = body

                    password = crypto.createHmac("sha256", password).update("Nikolay's crypto thing").digest("hex")
                    // console.log("data: ", account, name, email, password, description, department)
                    con.connect(function (err) {
                        if (err) throw err
                        // console.log("trying sql query")
                        // console.log("acc: ", account, "password: ", password)
                        con.query(`INSERT INTO users (account, name, email, password, description, department, status, session_key) VALUES ('${account}','${name}','${email}','${password}','${description}','${department}', '1', 'dummy')`, (err, result) => {
                            console.log("err: ", err)
                            console.log("result: ", result)

                            if (err) reject(err)
                            if (result) resolve(result)
                        })
                    })
                })
            //----------------Auth filter---------------------------
        } else if (reason.match(/filter$/)) {
            // console.log("trying login filter")
            const { id, sessionKey } = payload
            con.connect(function (err) {
                if (err) throw err
                // console.log("trying sql query", id)

                con.query(`SELECT * FROM users WHERE id='${id}' AND session_key='${sessionKey}'`, (err, result) => {
                    if (err) reject(err)
                    if (!result.length) reject("Error: Invalid password or account")
                    if (result && result.length) resolve(result)
                })
            })
            //----------------Auth filter---------------------------
        } else if (reason.match(/change-status$/)) {
            let body = ""
            payload
                .on("data", (chunk) => {
                    body = JSON.parse(chunk)
                })
                .on("end", () => {
                    const { id, status } = body
                    con.connect(function (err) {
                        if (err) throw err
                        con.query(`UPDATE users SET status='${status}' WHERE id='${id}'`, (err, result) => {
                            console.log("result: ", result)
                            if (err) reject(err)
                            if (result) resolve(result)
                        })
                    })
                })
        }
    })
}

// var crypto = require("crypto")

// var mykey = crypto.createCipher("aes-128-cbc", "mypassword")
// var mystr = mykey.update("abc", "utf8", "hex")
// mystr += mykey.final("hex")
