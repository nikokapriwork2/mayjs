const http = require("http")
const fs = require("fs")
const db = require("./api/db")
const authfilter = require("./api/authfilter")

http.createServer(async (req, res) => {
    const url = req.url === "/" ? "/page/home" : req.url
    // console.log("cookie", req.headers.cookie)
    //---------------Logout-----------------------
    if (url === "/api/logout") {
        console.log("trying logout")
        console.log("cookies on logout: ", req.headers.cookie)
        res.writeHead(301, {
            Location: "http://localhost:8080/page/login",
            "Cache-Control": "no-cache",
            "Set-Cookie": `${req.headers.cookie}; expires=${new Date().toUTCString()}; Path=/`
        })
        res.end()
        return
    }
    //---------------PAGES AUTH FILTER-----------------------
    if (url.includes("/page")) {
        const auth = await authfilter(req.headers.cookie)
        console.log("auth: ", auth)
        if (!auth && !["/page/login", "/page/registration"].includes(url)) {
            res.writeHead(301, {
                Location: "http://localhost:8080/page/login",
                "Cache-Control": "no-cache"
            })
            res.end()
            return
        } else if ((auth && ["/page/login", "/page/registration"].includes(url)) || (auth && !req.headers.cookie.match(/Ad\d+\D+-mayjs|HR\d+\D+-mayjs/) && url === "/page/userlist")) {
            res.writeHead(301, {
                Location: "http://localhost:8080/",
                "Cache-Control": "no-cache"
            })
            res.end()
            return
        }

        //---------------PAGE FILES-----------------------
        const page = fs.readFileSync(`.${url}.html`, "utf-8")
        res.setHeader("Content-Type", "text/html")

        res.write(page)

        res.end()

        //---------------DB RELATED-----------------------
    } else if (url.includes("/api")) {
        console.log("db type =", typeof db)
        db(url, req)
            .then((result) => {
                // console.log("type of result: ", typeof result)
                if (typeof result === "string") {
                    console.log("redirect becase of ", url)

                    if (result.includes("success")) res.writeHead(301, { Location: "http://localhost:8080/page/home" })
                    if (result.includes("updated")) res.writeHead(301, { Location: "http://localhost:8080/page/userlist" })

                    res.end()
                } else {
                    res.setHeader("Content-type", "application/json")

                    res.end(JSON.stringify(result))
                }
            })
            .catch((err) => {
                res.setHeader("Content-type", "application/json")
                res.end(JSON.stringify(`Error`))
            })
    } else if (url.includes("/static")) {
        //---------------STATIC JS-----------------------
        console.log("I M TRYING TO GET STATIC JS")
        const script = fs.readFileSync(`.${url}`)
        res.setHeader("Content-Type", "text/javascript")
        res.end(script)
    } else {
        // console.log("else url", url)
        //---------------OTHER FILES-----------------------
        if (url.match(/ico$/)) {
            const ico = fs.readFileSync(`./files${url}`)
            res.setHeader("Content-Type", "image/x-icon")

            res.end(ico)
        }
    }
}).listen(8080)
